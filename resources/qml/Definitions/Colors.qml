pragma Singleton
import QtQuick 2.0

QtObject {

  function rgba(r,g,b,a){
    return Qt.rgba(r/255. , g/255. , b/255. , a)
  }

  readonly property color background:            rgba (75 , 75 , 75 , 1.0)

  readonly property color seperator1:            rgba (180, 185, 188, 1.0)
  readonly property color seperator2:            rgba (138, 140, 142, 1.0)


  readonly property color grayText:              rgba (149, 152, 154, 1.0)
  
}
