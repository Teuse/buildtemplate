import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import Component 1.0 as Comp
import './Definitions'


ApplicationWindow {
  id: root
  visible: true
  width:   Screen.width
  height:  Screen.height
  color:   Color.background

  //------------------------------------------------------------------------------------------

  Comp.Test { id: testComponent }

  //------------------------------------------------------------------------------------------

  Image {
    source: "../images/build.png"
    anchors.horizontalCenter: parent.horizontalCenter 
    anchors.verticalCenter:   parent.verticalCenter 
  } 

}
