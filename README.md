# BuildTemplate #

This is a test project for my build configuration tool (bcm). It's qt app for iOS, iOS-Simulator and Mac but should be easily extendable for Win, Android, Linux, ... 

## Requirements ##

* python2
* pyYaml
* mod_pbxproj (this is python2 only)
* munch
* Qt 5.6
* boost

## Usage ##

Getting the sources:
```
#!bash
git clone --recursive https://Teuse@bitbucket.org/Teuse/buildtemplate.git
```

For configuration, execute the following commands from the project root dir:

Generate project for iOS:
```
#!bash
python bcm/configure.py -x ios
```


Generate project for Mac:
```
#!bash
python bcm/configure.py -x ios
```

Show help:
```
#!bash
python bcm/configure.py --help
```


## External Links ##

Icon/LaunchScreen Images:
http://doc.qt.io/qt-5/platform-notes-ios.html