# ------------------------------------------------------------------------------
# --- Jam Remote
# ------------------------------------------------------------------------------
# Do not run this script directly! Use bcm/configure.py instead.

# The following variables need to be set from outside
# - TARGET
# - CONFIGURE_PATH


# ------------------------------------------------------------------------------
# --- Configure Build
# ------------------------------------------------------------------------------

QT += qml quick widgets

CONFIG += c++14

QMAKE_CXXFLAGS += -fobjc-arc

ICON = $$_PRO_FILE_PWD_/resources/mac/Icon.icns

# ------------------------------------------------------------------------------
# --- iOS Setup
# ------------------------------------------------------------------------------

ios {
    QMAKE_INFO_PLIST = $$CONFIGURE_PATH/Info.plist

    ios_icon.files = $$files($$_PRO_FILE_PWD_/resources/ios/AppIcon*.png)
    QMAKE_BUNDLE_DATA += ios_icon

    app_launch_images.files = $$files($$_PRO_FILE_PWD_/resources/ios/LaunchImage*.png)
    QMAKE_BUNDLE_DATA += app_launch_images

    QMAKE_IOS_DEPLOYMENT_TARGET = 6.0
    QMAKE_IOS_TARGETED_DEVICE_FAMILY = "1,2"
}


# ------------------------------------------------------------------------------
# --- MAC Setup
# ------------------------------------------------------------------------------

macx {
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
}


# ------------------------------------------------------------------------------
# --- Collect files
# ------------------------------------------------------------------------------

INCLUDEPATH += $$_PRO_FILE_PWD_/source

HEADERS += source/GUI/QTest.h

SOURCES += source/main.cpp \
           source/GUI/QTest.cpp 

RESOURCES += resources/qml.qrc \
             resources/images.qrc

# ------------------------------------------------------------------------------
# --- Dependencies
# ------------------------------------------------------------------------------

ios:LIBS    += -L/usr/local/3rdparty/boost_1.61.0/lib_ios
macx:LIBS   += -L/usr/local/3rdparty/boost_1.61.0/lib_osx
INCLUDEPATH +=   /usr/local/3rdparty/boost_1.61.0

LIBS += -lboost \
        -framework Foundation
