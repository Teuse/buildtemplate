#include "QTest.h"

#include <iostream>

//------------------------------------------------------------------------------



QTest::QTest(QObject* parent)
: QObject( parent )
, _testBool(false)
{ }

//------------------------------------------------------------------------------

QTest::~QTest()
{ }

//------------------------------------------------------------------------------

bool QTest::testBool()     const { return _testBool; }

void QTest::testBool(bool b)
{
   if (_testBool != b) {
      _testBool = b;
      Q_EMIT testBoolChanged();
   }
}