#include <QObject>
#include <QtCore/QTimer>
#include <QVariantList>
#include <QString>


class QTest: public QObject
{
    Q_OBJECT
	Q_PROPERTY(bool         testBool     READ testBool     WRITE testBool     NOTIFY testBoolChanged )    

public:
    explicit QTest(QObject* parent = nullptr);
    ~QTest();
    
     bool          testBool() const;
     void          testBool(bool);
     Q_SIGNAL void testBoolChanged();

private:
    
    bool _testBool;
};

