#include <GUI/QTest.h>

#include <QtQml>
#include <QtCore/QByteArray>
#include <QtGui/QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtCore/QDir>
#include <QIcon>

#include <boost/predef.h>


//------------------------------------------------------------------------------------------------

void loadComponents()
{
   qmlRegisterType< QTest> ("Component", 1, 0, "Test");

}

//------------------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
#if BOOST_OS_WINDOWS
   // Force the Qt renderer to run in a separate thread (this is default behavior on Mac).
   qputenv("QSG_RENDER_LOOP", QByteArray("threaded"));
#endif

   QGuiApplication app(argc, argv);
   app.setApplicationVersion( QString("0.1") );
   //app.setWindowIcon(QIcon(":/mac/Icon.ico"));

   loadComponents();

   QQmlApplicationEngine engine;
   engine.load( QUrl("qrc:///qml/main.qml") );

   return app.exec();
}
